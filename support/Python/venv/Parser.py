import json

def get_status(status, component):
    tmp = json.loads(status)
    return str(tmp[component])

# status = '{"this": 1, "that": 2}'
# print(get_status(status,"this"))